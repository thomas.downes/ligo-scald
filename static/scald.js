var _global_plots = [];

function updateClock() {
 	var currentTime = new Date ( );
  	var currentHours = currentTime.getHours ( );
  	var currentMinutes = currentTime.getMinutes ( );
  	var currentSeconds = currentTime.getSeconds ( );

  	// Pad the minutes and seconds with leading zeros, if required
  	currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
  	currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

  	// Choose either "AM" or "PM" as appropriate
  	var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

  	// Convert the hours component to 12-hour format if needed
  	currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

  	// Convert an hours component of "0" to "12"
  	currentHours = ( currentHours == 0 ) ? 12 : currentHours;

  	// Compose the string for display
  	var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;

	var timeInMs = "" + Math.floor((Date.now() - 315964800000 + 17000)/1000.) + "&nbsp;" + currentTimeString;
  	$("#clock").html(timeInMs);
   	//$("#clock").html(currentTimeString);

 }

$(document).ready(function()
{
	setInterval('updateClock()', 1000);
});

function default_layout() {
	return {
		displayModeBar: false,
		margin: {t:15, b:20, l:30, r:0},
		font: {
			family: 'Noto Serif TC',
			size: 11,
//			color: 'rgb(0,0,0)'
		},
		xaxis: {tickformat: "d"},
	}
}

function default_options() {
	return {
		displayModeBar: false,
	}
}

function default_series_data_options() {
	return {
		line: {
			width: 0.5,
			},
		marker: {
//			color: 'rgb(3,3,3)',
			},
		colorscale: "Viridis",
	}
}

function duration_to_dt(duration) {
	if (duration <= 1000) {
		return 1;
	} else if (duration <= 10000) {
		return 10;
	} else if (duration <= 100000) {
		return 100;
	} else if (duration <= 1000000) {
		return 1000;
	} else if (duration <= 10000000) {
		return 10000;
	} else {
		return 100000;
	}
}

function gpsnow() {
	//FIXME doesn't account correctly for leapseconds (assumes 18)
	return Math.round(Date.now() / 1000.) - 315964800 + 18;
}

function update_config_object(old_object, new_object) {
	for (var key in new_object) {
		old_object[key] = new_object[key];
	}
	return old_object;
}

class _TimePlot {
	constructor(divname, measurement, schema, script_name, segment, refresh_interval = -1, data_options = {}, layout = {}, options = {}, delay = 0) {
		this.okay_to_draw = true;
		this.div = document.getElementById(divname);
		this.latest = 0;
		this.data = [];
		this.measurement = measurement;
		//this.schema = JSON.parse(schema.replace(/(&quot\;)/g,"\""));
		this.schema = schema;
		this.delay = delay;
		this.segment = segment;
		// save a reference to the initial duration, we will use that even if you are using live updates
		this.duration = segment.stop - segment.start;
		// determine dt from the duration set (used in aggregated data)
		this.schema['dt'] = duration_to_dt(this.duration);
		// set up url for querying data
		this.url = '';
		this.url_params = $.param(this.schema, true);
		this.base_url = new URL(`${script_name}api/timeseries/${this.measurement}/`, window.location.href);
		this.data_options = update_config_object(default_series_data_options(), data_options);
		// set plot options and intervals
		this.layout = update_config_object(default_layout(), layout);
		this.options = update_config_object(default_options(), options);
		this.refresh_interval = refresh_interval;
		this.prev_refresh_interval = refresh_interval;
	}

	populate() {
		// call this after plot is initialized to grab initial data
		this.get_data(this.segment);
		if (this.refresh_interval >0) {
			this.set_interval(this.refresh_interval);
		}
		_global_plots.push(this);
	}

	toggle_interval() {
		if (this.refresh_interval < 0) {
			this.set_interval(this.prev_refresh_interval);
		}
		else {
			this.clear_interval();
		}
		// Reset the query counters
		// NOTE because this is all asynchronous, things can get messed up if someone is pausing and unpausing rapidly
	}

	set_interval(interval) {
		this.refresh_interval = interval;
		var that = this;
		this.timer = setInterval(function() {that.increment_live_data();}, that.refresh_interval);
	}

	clear_interval() {
		this.refresh_interval = -1;
		clearInterval(this.timer);
	}

	get_data(segment = null) {
		if (this.okay_to_draw) {
			var that = this;
			if (!segment) {
				segment = this.segment;
			}
			this.url = new URL(`${segment.start}/${segment.stop}`, this.base_url)
			$.getJSON(this.url.href + '?' + this.url_params.replace(/&amp;/g, "&"), function(respdata) {that._draw(respdata);});
			this.okay_to_draw = false;
		}
	}

	increment_live_data() {
		var stop = gpsnow() - this.delay;
		// Keep the same duration as initially requested
		var start = stop - this.duration - this.delay;
		this.segment.stop = stop;
		this.segment.start = start;
		if (this.latest > this.segment.start) {
			start = this.latest;
		}
		this.get_data({"start":Math.floor(start), "stop":stop});
	}

	_new_data(respdata) {
		return {x:[], y:[], name:''};
	}

	// FIXME make a faster implementation
	_range(data, start, stop) {
		var i = 0;
		var newdata = this._new_data(data);
		// NOTE assumes x is time
		for (i=0; i < data.x.length; i++) {
			if (data.x[i] >= start && data.x[i] < stop) {
				newdata.x.push(data.x[i]);
				newdata.y.push(data.y[i]);
			}
		}
        newdata.name = data.name
		return newdata
	}

	_concat(d1, d2) {
		var xdata = d1.x.slice(0);
		var ydata = d1.y.slice(0);
		if (xdata.length == 0) {
			var maxt = 0;
		}
		else {
			var maxt = xdata[xdata.length -1];
		}
		var i;
		for (i = 0; i < d2.x.length; i++) {
			if (d2.x[i] > maxt) {
				xdata.push(d2.x[i]);
				ydata.push(d2.y[i]);
			}
		}
		return {x: xdata, y: ydata, name: d2.name}
	}

	_cat_data(d1, d2, segment) {
		return this._range(this._concat(d1,d2), segment.start, segment.stop);
	}

	_add_data_options(data) {
		for (var key in this.data_options) {
			data[key] = this.data_options[key];
		}
		return data;
	}

	_update_plot_data(respdata) {
		//
		// FIXME make sure that this is doing all of the right sanity checking, etc
		//
		var newdata = [];
		var earliest_latest = 0;
		var i;
		for (i = 0; i < respdata.length; i++) {
			var thisdata = this.data[i];
			if (typeof thisdata === "undefined") {
				thisdata = this._new_data(respdata[i]);
			}
			var thisnewdata = this._cat_data(thisdata, respdata[i], this.segment);
			newdata[i] = this._add_data_options(thisnewdata);
			if (earliest_latest == 0 && typeof newdata[i].x[newdata[i].x.length -1] != "undefined" || newdata[i].x[newdata[i].x.length -1] < earliest_latest) {
				earliest_latest = newdata[i].x[newdata[i].x.length -1];
			}
		}
		this.data = newdata;
		this.latest = earliest_latest;
	}

	_draw(respdata) {
		this.okay_to_draw = true;
		this._update_plot_data(respdata);
		Plotly.react(this.div, this.data, this.layout, this.options);
	}
}

class TimeSeries extends _TimePlot {
}

class TimeHeatMap extends _TimePlot {
	constructor(divname, measurement, schema, script_name, segment, refresh_interval = -1, data_options = {}, layout = {}, options = {}, delay = 0) {
		data_options.type = "heatmap";
		super(divname, measurement, schema, script_name, segment, refresh_interval, data_options, layout, options, delay);
		this.base_url = new URL(`${script_name}api/heatmap/${this.measurement}/`, window.location.href);
		this.earliest_idx = null;
	}

	_new_data(respdata) {
		var zd = [];
		var i;
		for (i=0; i<respdata.y.length; i++) {
			zd.push([]);
		}
		return {x:[], y:[], z:zd};
	}

	// FIXME make a faster implementation
	_range(data, start, stop) {
		var i = 0;
		var j = 0;
		var newdata = this._new_data(data);
		// NOTE assumes x is time
		for (i=0; i < data.x.length; i++) {
			if (data.x[i] >= start && data.x[i] < stop) {
				newdata.x.push(data.x[i]);
				for (j=0; j < data.z.length; j++) {
					newdata.z[j].push(data.z[j][i]);
				}
			}
			newdata.y = data.y // The y's should never change or else stuff will go really wrong.
		}
		return newdata
	}

	_concat(d1, d2) {
		var xdata = d1.x.slice(0);
		var ydata = d1.y.slice(0);
		var zdata = d1.z.slice(0);
		if (ydata.length == 0) {
			ydata = d2.y.slice(0); // y data should never change though the first time this is called there might not be anything set in d1
		}
		if (xdata.length == 0) {
			var maxt = 0;
		}
		else {
			var maxt = xdata[xdata.length -1];
		}
		var i;
		var j;
		for (i = 0; i < d2.x.length; i++) {
			if (d2.x[i] > maxt) {
				xdata.push(d2.x[i]);
				for (j=0; j< d2.z.length; j++) {
					zdata[j].push(d2.z[j][i]);
				}
			}
		}
		return {x: xdata, y: ydata, z: zdata}
	}

	_update_plot_data(respdata) {
		//
		// FIXME make sure that this is doing all of the right sanity checking, etc
		//
		var newdata = [];
		var earliest_latest = 0;
		var i;
		var j;
		var k;
		var l;
		for (i = 0; i < respdata.length; i++) {

			var thisdata = this.data[i];

			// remove part of heatmap with missing data if needed
			if (this.earliest_idx != null) {
				var latest_idx = thisdata.x.indexOf(this.latest);
				if (latest_idx != -1) {
					var maxslice = Math.min(latest_idx, thisdata.x.length);
					thisdata.x = thisdata.x.slice(0, maxslice);
					for (l = 0; l < thisdata.y.length; l++) {
						thisdata.z[l] = thisdata.z[l].slice(0, maxslice);
					}
				}
			}

			// add new data to current
			if (typeof thisdata === "undefined") {
				thisdata = this._new_data(respdata[i]);
			}
			var thisnewdata = this._cat_data(thisdata, respdata[i], this.segment);
			newdata[i] = this._add_data_options(thisnewdata);

			// find first time where there is missing data
			for (j = 0; j < respdata[i].x.length; j++) {
				for (k = 0; k < respdata[i].y.length; k++) {
					if (respdata[i].z[k][j] == null) {
						this.earliest_idx = j;
						earliest_latest = respdata[i].x[j];
						break;
					}
				}
				if (earliest_latest != 0) {
					break;
				}
			}
			if (earliest_latest == 0) {
				this.earliest_idx = respdata[i].x.length - 1;
				earliest_latest = respdata[i].x[respdata[i].x.length -1];
			}

		}
		this.data = newdata;
		this.latest = earliest_latest;
	}

}

class Bar extends _TimePlot {
	constructor(divname, measurement, schema, script_name, segment, refresh_interval = -1, data_options = {}, layout = {}, options = {}, delay = 0) {
		data_options.type = "bar";
		super(divname, measurement, schema, script_name, segment, refresh_interval, data_options, layout, options, delay);
		this.base_url = new URL(`${script_name}api/latest/${this.measurement}/`, window.location.href);
	}

	_update_plot_data(respdata) {
		var newdata = [];
		var i;
		for (i = 0; i < respdata.length; i++) {
			var thisdata = this.data[i];
			if (typeof thisdata === "undefined") {
				thisdata = this._new_data(respdata[i]);
			}
			newdata[i] = this._add_data_options(respdata[i]);
		}
		this.data = newdata;
	}
}

class Series extends _TimePlot {
	constructor(divname, measurement, schema, script_name, segment, refresh_interval = -1, data_options = {}, layout = {}, options = {}, delay = 0) {
		super(divname, measurement, schema, script_name, segment, refresh_interval, data_options, layout, options, delay);
		this.base_url = new URL(`${script_name}api/latest/${this.measurement}/`, window.location.href);
	}

	_update_plot_data(respdata) {
		var newdata = [];
		var i;
		for (i = 0; i < respdata.length; i++) {
			var thisdata = this.data[i];
			if (typeof thisdata === "undefined") {
				thisdata = this._new_data(respdata[i]);
			}
			newdata[i] = this._add_data_options(respdata[i]);
		}
		this.data = newdata;
	}
}

class IFAR extends _TimePlot {
	constructor(divname, measurement, schema, script_name, segment, refresh_interval = -1, data_options = {}, layout = {}, options = {}, delay = 0) {
		data_options = update_config_object({line: {shape: 'hv', width: 0.5}}, data_options);
		super(divname, measurement, schema, script_name, segment, refresh_interval, data_options, layout, options, delay);
		this.base_url = new URL(`${script_name}api/ifar/${this.measurement}/`, window.location.href);
	}

	_update_plot_data(respdata) {
		var newdata = [];
		var i;
		for (i = 0; i < respdata.length; i++) {
			var thisdata = this.data[i];
			if (typeof thisdata === "undefined") {
				thisdata = this._new_data(respdata[i]);
			}
			newdata[i] = this._add_data_options(respdata[i]);
		}
		this.data = newdata;
	}
}

import os
from setuptools import setup

setup(
    name = 'ligo-scald',
    description = 'SCalable Analytics for Ligo Data',
    version = '0.2.1',
    author = 'Patrick Godwin',
    author_email = 'patrick.godwin@ligo.org',
    url = 'https://git.ligo.org/gstlal-visualisation/ligo-scald.git',
    license = 'GPLv2+',

    packages = ['ligo', 'ligo.scald', 'ligo.scald.io', 'static'],
    namespace_packages = ['ligo'],

    package_data = {
        'static': ['*']
    },

    entry_points={
        'console_scripts': [
            'scald = ligo.scald.__main__:main',
        ],
    },
)

%define name              ligo-scald
%define version           0.2.1
%define unmangled_version 0.2.1
%define release           1

Summary:   SCalable Analytics for Ligo/virgo/kagra Data
Name:      %{name}
Version:   %{version}
Release:   %{release}%{?dist}
Source0:   %{name}-%{unmangled_version}.tar.gz
License:   GPLv2+
Group:     Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix:    %{_prefix}
Vendor:    Patrick Godwin <patrick.godwin@ligo.org>
Url:       https://git.ligo.org/gstlal-visualisation/ligo-scald

BuildArch: noarch
BuildRequires: rpm-build
BuildRequires: epel-rpm-macros
BuildRequires: python-rpm-macros
BuildRequires: python3-rpm-macros
BuildRequires: python-setuptools
BuildRequires: python%{python3_pkgversion}-setuptools

%description
ligo-scald is a gravitational-wave monitoring and dynamic data visualization
tool.

# -- python2-ligo-scald

%package -n python2-%{name}
Summary:  %{summary}
Provides: %{name}
Obsoletes: %{name}
Requires: python-six
Requires: python-future
Requires: python2-ligo-common
Requires: python2-bottle
Requires: python2-lal
Requires: python2-numpy
Requires: python2-pyyaml
Requires: h5py

%{?python_provide:%python_provide python2-%{name}}

%description -n python2-%{name}
ligo-scald is a gravitational-wave monitoring and dynamic data visualization
tool.

# -- python-3X-ligo-scald

%package -n python%{python3_pkgversion}-%{name}
Summary:  %{summary}
Requires: python%{python3_pkgversion}-six
Requires: python%{python3_pkgversion}-future
Requires: python%{python3_pkgversion}-ligo-common
Requires: python%{python3_pkgversion}-bottle
Requires: python%{python3_pkgversion}-lal
Requires: python%{python3_pkgversion}-numpy
Requires: python%{python3_pkgversion}-scipy
Requires: python%{python3_pkgversion}-pyyaml
Requires: h5py

%{?python_provide:%python_provide python%{python3_pkgversion}-%{name}}

%description -n python%{python3_pkgversion}-%{name}
ligo-scald is a gravitational-wave monitoring and dynamic data visualization
tool.

# -- build steps

%prep
%setup -n %{name}-%{unmangled_version}

%build
# build python3 first
%py3_build
# so that the scripts come from python2
%py2_build

%install
%py2_install
%py3_install

%clean
rm -rf $RPM_BUILD_ROOT

%files -n python2-%{name}
%license LICENSE
%{_bindir}/scald
%{python2_sitelib}/*

%files -n python%{python3_pkgversion}-%{name}
%license LICENSE
%{_bindir}/scald
%{python3_sitelib}/*

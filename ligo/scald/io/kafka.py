#!/usr/bin/env python

__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__description__ = "a module for kafka I/O utilities"

#-------------------------------------------------
### imports

import json
import logging

import numpy

from .. import aggregator

#-------------------------------------------------
### functions

def retrieve_timeseries(consumer, jobs, routes, timeout = 1000, max_records = 1000):
    """!
    A function to pull data from kafka for a set of jobs (topics) and
    routes (keys in the incoming json messages)
    """
    data = {route: {job: {'time': [], 'fields': {'data': []}} for job in jobs} for route in routes}

    ### retrieve timeseries for all routes and topics
    msg_pack = consumer.poll(timeout_ms = timeout, max_records = max_records)
    for tp, messages in msg_pack.items():
        job = tp.topic
        if job not in jobs:
            continue
        for message in messages:
            for route in routes:
                try:
                    data[route][job]['time'].extend(message.value[route]['time'])
                    data[route][job]['fields']['data'].extend(message.value[route]['data'])
                except KeyError: ### no route in message
                    pass

    ### convert series to numpy arrays
    for route in routes:
        for job in jobs:
            data[route][job]['time'] = numpy.array(data[route][job]['time'])
            data[route][job]['fields']['data'] = numpy.array(data[route][job]['fields']['data'])

    return data

#!/usr/bin/env python

__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__description__ = "tools to deploy web applications"

#-------------------------------------------------
### imports

from distutils import dir_util
import os
import pkg_resources
import shutil
import sys

from . import utils

#-------------------------------------------------
### script templates

CGI_BASH_TEMPLATE = """#!/bin/bash

export PYTHONPATH={python_path}:$PYTHONPATH
exec python -m ligo.scald serve -b cgi -c {config_path} -n {app_name} {cgi_extension}
"""

#-------------------------------------------------
### functions

def _add_parser_args(parser):
    parser.add_argument('-b', '--backend', default='cgi',
                        help="chooses server backend. options: [cgi]. default = cgi.")
    parser.add_argument('-c', '--config-path', required=True,
                        help="set path to specific plot and dashboard options based on yaml configuration.")
    parser.add_argument('-e', '--with-cgi-extension', default=False, action='store_true',
                        help="chooses whether scripts need to have a .cgi extension (if using cgi backend)")
    parser.add_argument('-o', '--output-dir', default=".",
                        help="chooses where web scripts and static files are deployed to (i.e. public_html)")
    parser.add_argument('-n', '--application-name',
                        help="chooses the web application name. default = yaml configuration name.")


def generate_cgi_script(config_path, output_dir, extension=False, script_name=None):
    if not script_name:
        script_name = os.path.splitext(os.path.basename(config_path))[0]
    if extension:
        script_name += '.cgi'
        cgi_extension = '-e'
    else:
        cgi_extension = ''
    script_file = os.path.join(output_dir, 'cgi-bin', script_name)

    ### write script to disk
    with open(script_file, 'w') as f:
        f.write(CGI_BASH_TEMPLATE.format(
            python_path=os.getenv('PYTHONPATH'),
            config_path=os.path.abspath(config_path),
            cgi_extension=cgi_extension,
            app_name=os.path.splitext(script_name)[0],
        ))

    ### change permissions so it can be executed by web server
    os.chmod(script_file, 0o755)

#-------------------------------------------------
### main

def main(args=None):
    """Deploys a web application

    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    config_path = args.config_path
    server_backend = args.backend
    output_dir = args.output_dir
    extension = args.with_cgi_extension
    app_name = args.application_name

    ### generate web scripts
    if server_backend == 'cgi':
        generate_cgi_script(config_path, output_dir, extension=extension, script_name=app_name)

    ### copy over static files
    static_dir = pkg_resources.resource_filename(pkg_resources.Requirement.parse('ligo_scald'), 'static')
    try:
        dir_util.copy_tree(static_dir, os.path.join(output_dir, 'static'))
    except OSError:
        pass

#!/usr/bin/env python

__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__description__ = "a module to store web-based templates"

#-------------------------------------------------
### templates

HEADER_TEMPLATE = """
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>{{title}}</title>

  <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />

  <!-- Dashboard -->
  <link href="{{static_dir}}static/scald.css" rel="stylesheet" type="text/css" />

  <!-- Dependencies -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />


  <!-- Scripts -->
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif+TC" rel="stylesheet">
  <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
  <script
      src="https://code.jquery.com/jquery-3.3.1.min.js"
      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
      crossorigin="anonymous">
  </script>
  <script src="{{static_dir}}static/scald.js"></script>

</head>
"""

BODY_HEADER_TEMPLATE = """
<body class="keen-dashboard" style="padding-top: 80px;">
"""

DASHBOARD_NAVBAR_TEMPLATE = """
  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        % if image:
          <a class="navbar-brand" href="#"><img style="width:80px; margin-top:-8px" src="{{image}}"></a>
        % else:
          <a class="navbar-brand" href="">{{title}}</a>
        % end
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-left">
        % for tab in tabs:
        %   tab_title = tab['name']
        %   tab_url = tab['url']
          <li><a href="{{tab_url}}">{{tab_title}}</a></li>
        % end
        </ul>
        <form class="nav navbar-nav navbar-form navbar-center" role="search">
          <div class="form-group">
            <input type="text" class="form-control" name="GPS" value="{{gps}}" size=12>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="Duration" value="{{duration}}" size=6 >
          </div>
          <div class="form-group dropdown">
            <button class="btn btn-default dropdown-toggle" type="button"
                    id="dropdownMenu1" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="true">
              <i class="glyphicon glyphicon-stats"></i>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu checkbox-menu allow-focus" aria-labelledby="dropdownMenu1">
            % for plot in plots:
            %   plot_title = plot['title']
            %   plot_id = plot['title'].replace(' ', '_').lower()
            %   plot_value = plot['value']
              <li >
                <label>
                  <input type="checkbox" name="livecharts{{plot_id}}" {{plot_value}}>{{plot_title}}
                </label>
              </li>
            % end
            </ul>
          </div>
          <!--<div class="form-group">
            <select value="Directory" name="Directory" class="form-control">
              <option value="/path/to/analysis" selected>/path/to/analysis</option>
            </select>
          </div>-->
          <div class="form-group">
            <input type="submit" value="Submit" class="form-control">
          </div>
        </form>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#"><div id="clock" style="color: grey"></div></a></li>
        </ul>
      </div>
    </div>
  </div>
"""
DASHBOARD_PLOT_TEMPLATE = """
  <div class="container-fluid">

  % for plot in plots:
  %   plot_title = plot['title']
  %   plot_id = plot['title'].replace(' ', '_').lower()
  %   if plot['value'] == "checked":
    <div class="row">
      <div class="col-sm-12">
        <div class="chart-wrapper">
          <div class="chart-title">
            {{plot_title}}
          </div>
          <div class="chart-stage"><div id="{{plot_id}}"></div></div>
          <!-- <div class="chart-notes">
            Notes about this chart
          </div> -->
        </div>
      </div>
    </div>
  %   end
  % end

  </div>
<script>

  % import json
  % for ii, plot in enumerate(plots, 1):
  %     plot_id = plot['title'].replace(' ', '_').lower()
  %     plot_type = plot['type']
  %     measurement = plot['measurement']
  %     schema = json.dumps(plot['schema'])
  %     if plot['value'] == 'checked':
            var plot{{ii}} = new {{plot_type}}(
                "{{plot_id}}",         // Div name for the plot
                '{{measurement}}',
                {{!schema}},
                '{{script_name}}',     // script name (hack to deal with cgi/apache)
                {"start":  {{start}}, "stop": {{stop}} }, // Initial data segment
                {{refresh}},           // refresh interval in ms, -1 to disable refresh
  %             if 'dataOptions' in plot:
  %                 plot_dataOptions = plot['dataOptions']
                    {{!plot_dataOptions}},     // custom plot data options, provided as a dict
  %             else:
                    {}, // default data options
  %             end
  %             if 'layout' in plot:
  %                 plot_layout = plot['layout']
                    {{!plot_layout}},       // custom plot layout, provided as a dict
  %             else:
                    {}, // default layout
  %             end
  %             if 'options' in plot:
  %                 plot_options = plot['options']
                    {{!plot_options}},     // custom plot options, provided as a dict
  %             else:
                    {}, //default options
  %             end
                    {{delay}},             // sets delay for realtime data
            );
            plot{{ii}}.populate();
  %     end
  % end

  document.body.onkeydown = function(e) {
    if (e.keyCode == 32) {
      e.preventDefault();
    }
  };

  document.body.onkeyup = function(e){
    if(e.keyCode == 32){
      var i;
      for (i = 0; i < _global_plots.length; i++) {
        _global_plots[i].toggle_interval();
      }
    }
  };

</script>
"""

BODY_FOOTER_TEMPLATE = """
</body>
</html>
"""

#!/usr/bin/env python

__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__description__ = "tools to serve data and dynamic html pages"

#-------------------------------------------------
### imports

import copy
import functools
import json
import os
import pkg_resources
import sys
import time

from six.moves import urllib

import bottle
import yaml

from . import aggregator
from . import io
from . import templates
from . import utils

#-------------------------------------------------
### constants

JSON_HEADER = {
    'Content-type': 'application/json',
    'Cache-Control': 'max-age=10',
}


#-------------------------------------------------
### bottle apps/routing

def static(file_):
    static_dir = pkg_resources.resource_filename(pkg_resources.Requirement.parse('ligo_scald'), 'static')
    yield bottle.static_file(file_, root=static_dir)


def index(config, script_name='', use_cgi=False):
    config = copy.deepcopy(config)
    static_dir = '../' if use_cgi else ''

    # check if the user has checked any plots, otherwise use default values
    if any(["livecharts" in k for k in bottle.request.query]):
        for plot in config['plots']:
            plot['value']='checked' if "livecharts%s" % plot['title'].replace(' ', '_').lower() in bottle.request.query else ''

    if "gps" in bottle.request.query:
        config['dashboard']['gps']=bottle.request.query['gps']
    if "duration" in bottle.request.query:
        config['dashboard']['duration']=bottle.request.query['duration']

    ### generate html
    yield bottle.template(templates.HEADER_TEMPLATE, title=config['dashboard']['title'], static_dir=static_dir)

    yield bottle.template(templates.BODY_HEADER_TEMPLATE)
    yield bottle.template(
        templates.DASHBOARD_NAVBAR_TEMPLATE,
        static_dir=static_dir,
        plots=config['plots'],
        **config['dashboard']
    )


    ### find out whether query is for realtime or historical data
    ### NOTE: gps < 0 is used as a proxy for realtime data and refers
    ###       to the delay (i.e. -5 means to query up to now - 5s
    gpstime = int(config['dashboard']['gps'])
    if gpstime < 0:
        delay = -gpstime
        stop = aggregator.now() - delay
        refresh = 2000
    else:
        delay = 0
        stop = gpstime
        refresh = -1
    start = stop - int(config['dashboard']['duration'])

    yield bottle.template(
        templates.DASHBOARD_PLOT_TEMPLATE,
        plots=config['plots'],
        start=start,
        stop=stop,
        refresh=refresh,
        delay=delay,
        script_name=script_name,
    )

    yield bottle.template(templates.BODY_FOOTER_TEMPLATE)


def serve_timeseries(measurement, start, end, config):
    column, tags, tag_filters, aggregate, dt, _, datetime = parse_query(bottle.request.query)

    consumer = config_to_consumer(config['database'])
    response = []

    ### query for timeseries
    if tag_filters:
        for tag in tag_filters:
            time, data = consumer.retrieve_timeseries(measurement, start, end, column, tags=[tag], aggregate=aggregate, dt=dt, datetime=datetime)
            response.append({'x':time, 'y':data, 'name': tag[1]})
    else:
        time, data = consumer.retrieve_timeseries(measurement, start, end, column, tags=tag_filters, aggregate=aggregate, dt=dt, datetime=datetime)
        response.append({'x':time, 'y':data})

    ### return data
    return bottle.HTTPResponse(status=200, headers=JSON_HEADER, body=json.dumps(response).replace("NaN","null"))


def serve_heatmap(measurement, start, end, config):
    column, tag, _, aggregate, dt, far, datetime = parse_query(bottle.request.query)

    consumer = config_to_consumer(config['database'])
    response = []

    ### query for timeseries
    times, tags, datum = consumer.retrieve_binnedtimeseries_by_tag(measurement, start, end, column, tag, aggregate=aggregate, dt=dt, datetime=datetime)

    ### format request
    response = [{'x':times, 'y':tags, 'z':datum}]

    ### return data
    return bottle.HTTPResponse(status=200, headers=JSON_HEADER, body=json.dumps(response).replace("NaN","null"))


def serve_latest(measurement, start, end, config):
    column, tags, tag_filters, aggregate, dt, _, datetime = parse_query(bottle.request.query)

    tag = config['measurements'][measurement]['tag']
    tag_ids = [str(tag_num).zfill(4) for tag_num in range(config['measurements'][measurement]['num_tags'])]
    default_value = config['measurements'][measurement]['default']
    transform = config['measurements'][measurement]['transform']
    y = []

    ### query for timeseries
    consumer = config_to_consumer(config['database'])
    for tag_id in tag_ids:
        time, data = consumer.retrieve_timeseries_latest(measurement, column, tags=[(tag, tag_id)], aggregate=aggregate, dt=dt, datetime=datetime)
        y.append(transform_data(time, data, transform, default_value, utils.gps_now()))

    ### format request
    response = [{'x':tag_ids, 'y':y}]

    ### return data
    return bottle.HTTPResponse(status=200, headers=JSON_HEADER, body=json.dumps(response))


def serve_ifar(measurement, start, end, config):
    columns, _, _, _, _, far, datetime = parse_query(bottle.request.query)

    consumer = config_to_consumer(config['database'])
    response = []

    ### query for timeseries
    triggers = consumer.retrieve_triggers(measurement, start, end, columns, far=far, datetime=datetime)

    ### format request
    fars = sorted([1./ row['far'] for row in triggers], reverse=True)
    response = [{'x':fars, 'y':range(1, len(fars))}]

    ### return data
    return bottle.HTTPResponse(status=200, headers=JSON_HEADER, body=json.dumps(response).replace("NaN","null"))


def serve_nagios(check, config):
    backend = config['database']['backend']
    nagios_config = config['nagios'][check]

    ### time settings
    duration = nagios_config['lookback']
    end = aggregator.now()
    start = end - duration
    dt = utils.duration_to_dt(duration)

    ### data settings
    measurement = nagios_config['measurement']
    column = nagios_config['column']
    tags = nagios_config['tags'] if 'tags' in nagios_config else []
    aggregate = nagios_config['aggregate']

    ### alert settings
    alert_type = nagios_config['alert_type']
    alert_tags = utils.extract_alert_tags(nagios_config['alert_settings'])

    ### alert tracking
    alert_values = []
    bad_status = 0
    now = utils.gps_now()

    ### retrieve data
    consumer = config_to_consumer(config['database'])
    for alert_tag in alert_tags:
        time, data = consumer.retrieve_timeseries(measurement, start, end, column, tags=[alert_tag].extend(tags), aggregate=aggregate, dt=dt)

        if alert_type == 'heartbeat':
            if time:
                alert_values.append(now - time[-1])
            else:
                bad_status += 1

        elif alert_type == 'threshold':
            if data:
                max_data = max(data)
                if max_data >= nagios_config['alert_settings']['threshold']:
                    bad_status += 1

    ### format nagios response
    if bad_status:
        if alert_type == 'heartbeat':
            text_status = "{num_tags} {alert_tag} more than {lookback} seconds behind".format(
                alert_tag=nagios_config['alert_settings']['tag_type'],
                num_tags=bad_status,
                lookback=duration,
            )
        elif alert_type == 'threshold':
            text_status = "{num_tags} {alert_tag} above {column} threshold = {threshold} {units} from gps times: {start} - {end}".format(
                alert_tag=nagios_config['alert_settings']['tag_type'],
                threshold=nagios_config['alert_settings']['threshold'],
                units=nagios_config['alert_settings']['threshold_units'],
                num_tags=bad_status,
                column=measurement,
                start=start,
                end=end,
            )

    else:
        if alert_type == 'heartbeat':
            text_status = "OK: Max delay: {delay} seconds".format(delay=max(alert_values))
        elif alert_type == 'threshold':
            text_status = "OK: No {alert_tag}s above {column} threshold = {threshold} {units} from gps times: {start} - {end}".format(
                alert_tag=nagios_config['alert_settings']['tag_type'],
                threshold=nagios_config['alert_settings']['threshold'],
                units=nagios_config['alert_settings']['threshold_units'],
                column=measurement,
                start=start,
                end=end,
            )

    ### return response
    response = utils.status_to_nagios_response(text_status, bad_status=bad_status)
    return bottle.HTTPResponse(status=200, headers=JSON_HEADER, body=json.dumps(response, sort_keys=True, indent=4, separators=(',', ': ')))


#-------------------------------------------------
### functions

def parse_query(query):
    columns = query['column']
    tags = query.get('tag', [])

    dt = int(query.get('dt', None))
    far = query.get('far', None)
    aggregate = query.get('aggregate', None)

    if 'datetime' in query:
        datetime = query['datetime'] == 'true'
    else:
        datetime = False

    tag_filters = [(key, val) for key, val in bottle.request.query.allitems() if key in tags]

    return columns, tags, tag_filters, aggregate, dt, far, datetime


def transform_data(time, data, transform, default_value, now):
    if transform == 'none':
        if data:
            return data[-1]
        else:
            return default_value
    elif transform == 'latency':
        if time:
            return now - time[-1]
        else:
            return default_value
    else:
        raise NotImplementedError('transform option not known/implemented')



def config_to_consumer(config):
    backend = config['backend']
    if backend == 'influxdb':
        return io.influx.Consumer(**config)
    elif backend == 'hdf5':
        return io.hdf5.Consumer(**config)
    else:
        raise NotImplementedError


def _add_parser_args(parser):
    parser.add_argument('-c', '--config', required=True,
                        help="sets specific plot and dashboard options based on yaml configuration.")
    parser.add_argument('-b', '--backend', default='wsgiref',
                        help="chooses server backend. options: [cgi|wsgiref]. default=wsgiref.")
    parser.add_argument('-e', '--with-cgi-extension', default=False, action='store_true',
                        help="chooses whether scripts need to have a .cgi extension (if using cgi backend)")
    parser.add_argument('-n', '--application-name',
                        help="chooses the web application name. default = yaml configuration name.")


#-------------------------------------------------
### main

def main(args=None):
    """Serves data and dynamic html pages

    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    ### parse args and set up configuration
    server_backend = args.backend
    app_name = args.application_name

    ### hacks to deal with running on apache and/or cgi
    use_cgi = (server_backend == 'cgi')

    if not app_name:
        app_name = os.path.splitext(os.path.basename(args.config))[0]
    if args.with_cgi_extension:
        app_name += '.cgi/'
    else:
        app_name += '/'
    script_name = app_name if use_cgi else ''

    ### load config file
    config = None
    with open(args.config, 'r') as f:
        config = yaml.safe_load(f)

    ### set up routes
    app = bottle.Bottle()
    app.route("/", "GET", functools.partial(index, config=config, use_cgi=use_cgi, script_name=script_name))
    app.route("/api/timeseries/<measurement>/<start:int>/<end:int>", "GET", functools.partial(serve_timeseries, config=config))
    app.route("/api/heatmap/<measurement>/<start:int>/<end:int>", "GET", functools.partial(serve_heatmap, config=config))
    app.route("/api/latest/<measurement>/<start:int>/<end:int>", "GET", functools.partial(serve_latest, config=config))
    app.route("/api/ifar/<measurement>/<start:int>/<end:int>", "GET", functools.partial(serve_ifar, config=config))
    app.route("/api/nagios/<check>", "GET", functools.partial(serve_nagios, config=config))

    ### serve static files directly
    if server_backend == 'wsgiref':
        app.route("/static/<file_>", "GET", static)

    ### start server
    bottle.run(app, server=server_backend, debug=True)
